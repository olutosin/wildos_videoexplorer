$(document).ready(function(){
	//INITIALIZE
	var video = $('#myVideo');
	var bluescreen = $('#bluescreenDiv');
 
	//before everything get started
	video.on('loadedmetadata', function() {

		//initTime(70,30);
		// starttime = 70;
		// maxduration = 30; 

		// set video current time to desired start time
		video[0].currentTime = starttime;

		//set video properties on progress bar
		$('.current').text(timeFormat(0));
		$('.duration').text(timeFormat(maxduration));
	});
	
	//display current video play time
	video.on('timeupdate', function() {

		var currentPos = video[0].currentTime - starttime;
		var perc = 100 * currentPos / maxduration;
		$('.timeBar').css('width',perc+'%');	
		$('.current').text(timeFormat(currentPos));

		//If current time is greater than start time plus duration, pauses the video.
		if(currentPos > maxduration){ 
		    video[0].pause();
		    video[0].currentTime = starttime;
		    bluescreen.show();
			setTimeout(playpause,2000);
		  }
	});
	
	//CONTROLS EVENTS
	//video screen and play button clicked
	video.on('click', function() { playpause();} );
	
	var playpause = function() {
		if(video[0].paused || video[0].ended) { // use maximum duration
			video[0].play();
			bluescreen.hide();
			/*bluescreen.toggle();*/
		}
		else {
			video[0].pause();
			bluescreen.show();
		}
	};
	
	//VIDEO EVENTS
	//video ended event
	// video.on('ended', function() {
	// 	bluescreen.show();
	// 	setTimeout(playpause,2000);

	// });
	
	//video canplaythrough event
	//solve Chrome cache issue
	var completeloaded = false;
	video.on('canplaythrough', function() {
		completeloaded = true;
	});

	//video seeking event
	video.on('seeking', function() {
		//if video fully loaded, ignore loading screen
		if(!completeloaded) { 
			$('.loading').fadeIn(200);
		}	
	});
	
	//video seeked event
	video.on('seeked', function() { });
	
	//VIDEO PROGRESS BAR
	//when video timebar clicked
	var timeDrag = false;	/* check for drag event */
	$('.progress').on('mousedown', function(e) {
		timeDrag = true;
		updatebar(e.pageX);
	});
	$(document).on('mouseup', function(e) {
		if(timeDrag) {
			timeDrag = false;
			updatebar(e.pageX);
		}
	});
	$(document).on('mousemove', function(e) {
		if(timeDrag) {
			updatebar(e.pageX);
		}
	});

});

//get these two as argu from tile and pass to this place to fix the max duration.
var maxduration = 0;//video[0].duration; // use parameter from tile not video.duration
var starttime = 0;

//
function updatebar(x) {
	var progress = $('.progress');
	//calculate drag position and update video currenttime
	//as well as progress bar
	var position = x - progress.offset().left;
	var percentage = 100 * position / progress.width();
	if(percentage > 100) {
		percentage = 100;
	}
	if(percentage < 0) {
		percentage = 0;
	}
	$('.timeBar').css('width',percentage+'%');	
	video[0].currentTime = maxduration * percentage / 100;
};

function timeFormat(seconds){
	var m = Math.floor(seconds/60)<10 ? "0"+Math.floor(seconds/60) : Math.floor(seconds/60);
	var s = Math.floor(seconds-(m*60))<10 ? "0"+Math.floor(seconds-(m*60)) : Math.floor(seconds-(m*60));
	return m+":"+s;
};

function resetVideo(){
	var m = Math.floor(seconds/60)<10 ? "0"+Math.floor(seconds/60) : Math.floor(seconds/60);
	var s = Math.floor(seconds-(m*60))<10 ? "0"+Math.floor(seconds-(m*60)) : Math.floor(seconds-(m*60));
	return m+":"+s;
};

function initTime(s,m){
	starttime = s;
	maxduration = m; //video[0].duration;
};







