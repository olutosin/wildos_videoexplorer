// VideoExplorer app - displays a web page 

// Shared modules
var OO = require('OO');
var log = require('Log').shared();

// Renderer modules
var App = require('../lib/App');


// The `VideoExplorer` class.
var VideoExplorer = App.subclass().name('VideoExplorer')
	.fields({
		//
	})
	.constructor(function(config) {
		this._super(config);

		// The video element
		 this.video = null;
		
		// Monitor state to adjust presentation
		this.wrapFields({
			//
		});
	})
	.methods({

		// Called by the framework when the webpage representing the tile is ready
		tileReady: function(tile) {
			if (!tile.window)
				return;

			this.setTileHREF(tile);

			// Set a permanent handler for subsequent loads.
			// Note that this works too when a page has an auto-refresh.
			var self = this;
			tile.window.once('loaded', function() {
				log.warn.enter(self, 'loaded');
				self.loaded(tile);
				log.warn.exit(self, 'loaded');
			});
		},
		
		// Called when the content of the window is loaded and ready.
		loaded: function(tile) {
			if (!tile || !tile.window || !tile.window.window)
				return;

			// Get tile html document to be able to retrieve elements in the doc
			var doc = tile.window.window.document;

			this.video = doc.getElementById('myVideo');

			var videourl = 'app://localhost/content/videoExplorer/tile/ATCs.ogv';
			//var videourl = 'file:///Users/olutosin/Desktop/WildOS-V2.1/renderer/content/videoExplorer/tile/ATCs.ogv';

			// Get the video element and set its src video
			this.video.setAttribute('src', videourl);

			if (tile.window.window.initTime)
				tile.window.window.initTime(120, 10);
		},

		
		// Change the URL.
		setTileHREF: function (tile) {

			if (!tile || !tile.ready || !tile.window){
					log.method(this, 'setTileHREF', '- tile is null, or not ready, or no tile window');
					return;
				}

			log.method(this, 'setTileHREF','this.video');

			//// This will trigger a loaded event that will be handled above.
			if (tile && tile.ready && tile.window)
				tile.window.window.location.href = 'app://localhost/content/videoExplorer/tile/videoTile.html';
				//tile.window.window.location.href = 'file:///Users/olutosin/Desktop/WildOS-V2.1/renderer/content/videoExplorer/tile/videoTile.html';
		},

		//
		pauseVideo_after: function() { //pass tile id as argument which is gotten from server

			var name = this.server.host+'_'+this.server.instance;

			if(this.server.host == 'a1' && this.server.instance == 'R'){

			if (this.video.paused) 
          		this.video.play();
       		else 
          		this.video.pause();
          }

		},

		playPauseAll_after: function() {
			if (this.video.paused) 
          		this.video.play();
       		else 
          		this.video.pause();
         },

	})
	.shareState()
;

log.spyMethods(VideoExplorer);

module.exports = VideoExplorer;
