// VideoExplorer app - displays a web page 
//
// Simple example of application:
// Load a page in the surface and pan/zoom it from the control interface.
//

// Shared modules
var OO = require('OO');
var log = require('Log').shared();

// Renderer modules
var App = require('../lib/App');

// The `VideoExplorer` class.
var VideoExplorer = App.subclass().name('VideoExplorer')
	.fields({
		// background page + its offset and zoom factor
		url: null,		// URL of the page to load
		offsetX: 0,		// Offset of the page from the topleft of the surface
		offsetY: 0,				
		zoom: 1,		// Zoom factor
		video: null,
	})
	.constructor(function(config) {
		this._super(config);

		// Monitor state to adjust presentation
		this.wrapFields({
			set url(url)   { this._set(url); this.adjustURLs(); },
			set video(video)   { this._set(video); this.adjustURLs();},
			set offsetX(x) { this._set(x); this.adjustPositions(); },
			set offsetY(y) { this._set(y); this.adjustPositions(); },
			set zoom(z)    { this._set(z); this.adjustPositions(); },
		});
	})
	.methods({
		// Called by the framework when the webpage representing the tile is ready
		tileReady: function(tile) {
			if (!tile.window)
				return;

			if (this.url)
				this.adjustURL();

			if (this.video)
				this.adjustURL();

			// Set a permanent handler for subsequent loads.
			// Note that this works too when a page has an auto-refresh.
			var self = this;
			tile.window.on('loaded', function() {
				log.enter(self, 'loaded');
				self.loaded(tile);
				log.exit(self, 'loaded');
			});
		},
		
		// Called when the content of the window is loaded and ready.
		loaded: function(tile) {
			// Adjust the style of the body.
			/*var style = tile.window.window.document.body.style;
			style.position = "absolute";
			style.overflow = "hidden";
			style.width = "1000px";

			// Adjust the tile origin.
			this.adjustPosition(tile); */
		},

		// Adjust positions of each tile
		adjustURLs: function() {
			var self = this;
			this.mapTiles(function(tile) {
				self.adjustURL(tile);
			});
		},

		// Change the URL.
		adjustURL: function (tile) {
			if (!tile || !tile.ready || !tile.window) {
				log.method(this, 'adjustURL', '- tile not ready');
				return;
			}
			log.method(this, 'adjustURL',this.url);
/*
			if (tile && tile.ready && tile.window)
				tile.window.window.location.href = 'app://localhost/content/videoExplorer/tile/videoTile.html';

			
			// Get tile html document that has just been set above
			var doc = tile.window.window.document;

			//var starttime = 400;
			//var endtime = 430;

			//video element will be inside bluescreen div
			//var videourl = 'app://localhost/content/videoExplorer/videos/ATCs.ogv#t='+starttime+','+endtime;

			// Get the video element and set its src video
			//var videoElement = doc.getElementById(myVideo);
			//videoElement.setAttribute('src', videourl);
			//videoElement.play();
			
			this.video = doc.getElementById(myVideo);	*/



			// Get tile html document
			var doc = tile.window.window.document;

			var videourl = 'app://localhost/content/videoExplorer/videos/ATCs.ogv';			

			var videoElementID = 'VideoID_'+this.id;

			var videoElement = doc.createElement('video');
				videoElement.setAttribute('id', videoElementID);
				//videoElement.setAttribute('controls', 'controls');
				//videoElement.setAttribute('poster', poster.jpg);
				videoElement.setAttribute('loop', 'loop');
				videoElement.setAttribute('autoplay', 'autoplay');
				videoElement.setAttribute('src', videourl);
				videoElement.setAttribute('style', 'position:absolute; margin:0px; width:100%; height:100%;');
				videoElement.style.left = tile.originX +'px';
				videoElement.style.top =  tile.originY +'px';
				doc.body.appendChild(videoElement);

			this.video = videoElement;
		},

		// Adjust positions of each tile
		adjustPositions: function() {
			var self = this;
			this.mapTiles(function(tile) {
				self.adjustPosition(tile);
			});
		},

		// Adjust the offset of the body element to view the proper part 
		// of the content and create the mosaic effect
		adjustPosition: function (tile) {
			if (!tile || !tile.ready || !tile.window) {
				log.method(this, 'adjustPosition', '- tile not ready');
				return;
			}
			log.method(this, 'adjustTile'+this.offsetX+' '+this.offsetY+' - '+tile.originX+' '+tile.originY+' - '+this.zoom);
/*
			var style = tile.window.window.document.body.style;
			style.left = Math.round((this.offsetX-tile.originX)/this.zoom)+"px";
			style.top = Math.round((this.offsetY-tile.originY)/this.zoom)+"px";
			style.zoom = Math.round(this.zoom*100,2)+'%';
			*/
		},

		pauseVideo_after: function() {
			this.video.pause();			
		},

	})
	.shareState()
;

log.spyMethods(VideoExplorer);

module.exports = VideoExplorer;
