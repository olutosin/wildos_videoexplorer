// Code injected in the platform control window
//
// Here we let the user load pages and control the position and zoom factor 
// of the page being displayed on the wall.

var app = platform.apps.getApp('VideoExplorer');

// The miniature wall position and scale factor
var wall = {
	originX: 0,
	originY: 0,
	width: 800,
	height: 600,
	zoom: 1.0
};

// Adjust the frame showing the page.
function adjustLocal() {
	
	var zoom = wall.zoom*app.zoom; // accounts for wallZoom in wild.html
	var percentZoom = Math.round(zoom*100,2)+'%';
	$('#videoPage')
		.css('left', Math.round(app.offsetX/app.zoom)+"px")
		.css('top',  Math.round(app.offsetY/app.zoom)+"px")
		.css('zoom', percentZoom)
		.contents().find('body').css('zoom', percentZoom);
	
}

// Called at the end of the script.
function startVideoExplorer() {
	// Create a div that will hold the app's UI, so we can easily remove it
	$('#top').append('<div id="videoExplorerApp">VideoExplorer: </div>');

	//
	$('#videoExplorerApp').append('<br />');
	$('#videoExplorerApp').append('<div>Presentation: <select> <option value="WAVE">WAVE</option> <option value="LOOP">LOOP</option> </select> Num of Screens: <select style="width:100px;"> <option value="10">10</option> <option value="20">20</option> <option value="30">30</option></select> <input id="startPlaying" type="button" value="Start"/> <input id="playVid" type="button" value="Play2"/> </div> ');
	//$('#videoExplorerApp').append('<div></div> ');
	$('#startPlaying').click(function() { app.pauseVideo(); });
	$('#playVid').click(function() { app.playPauseAll(); });

	// Get the wall position / zoom factor
	var config = platform.config.UI;
	if (config)
		wall.zoom = config.wallZoom || 1.0;
	var wallOffset = $('#wall').offset();
	wall.originX = wallOffset.left;
	wall.originY = wallOffset.top;
	var surface = platform.findDevice({type: 'Surface'});
	if (surface) {
		wall.width = surface.width;
		wall.height = surface.height;
	}


	var size = 'width:100%; height:90%;';

	$('#wall').append('<div id="videoPage" style="z-index: 1; position: absolute; overflow: hidden; background-color:#194AD1;'+size+'" scrolling="no"/>');
	$('#videoPage').load(adjustLocal);


	// Update URL when changed in the application
	app.wrapFields({
		//
	});
}

function stopVideoExplorer() {
	// Remove the elements that we added
	$('#videoExplorerApp').remove();
	$('#videoExplorerJS').remove();
}
// Go!
startVideoExplorer();






