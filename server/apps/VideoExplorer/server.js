// command line arguments and other module dependencies
var stdio = require('stdio')
, path = require('path')
, fs = require('fs');

var ops = stdio.getopt({
    'range': {key: 'r', args: 1, description: 'range of files from a directory with visuals\n'+
        '\t\t\tgiven as numerals; either sequential, e.g. 1-n,\n'+
        '\t\t\tor arbitrary numbers separated by commas that can\n'+
        '\t\t\tbe non-sequential and/or have duplicates,\n'+
        '\t\t\te.g.: 1,3,7,...,n; 1,1,1,1; 3,,,2,,,5;\n'+
        '\t\t\tnumber k in the i-th comma-separated position\n'+
        '\t\t\tcorresponds to the k-th file in an alphanumerically\n'+
        '\t\t\tsorted list of video files found in a directory\n'+
        '\t\t\twith visuals; this file will be displayed in\n'+
        '\t\t\tthe i-th grid cell assuming cells are ordered\n'+
        '\t\t\tleft-to-right, top-to-bottom\n'},
    'files': {key: 'f', args: 1, description: 'file names without extensions as a,b,c,...,n\n'+
        '\t\t\t(alphanumerals, no spaces, or with spaces but in quotes\n'+
        '\t\t\te.g. "a, b, c")\n'+
        '\t\t\t-r and -f are mutually exclusive and -f has the priority\n'},
    'path': {key: 'p', args: 1, description: 'ABSOLUTE path to a directory with visuals\n'+
        '\t\t\t(default: ./visuals/ with respect to the server root)\n'},
    'config': {key: 'c', args: 1, description: 'name of the configuration file\n'+
        '\t\t\t(has to be located within the directory with visuals),\n'+
        '\t\t\tcontains information about the videos, their positions,\n'+
        '\t\t\tcategories, and videos tagged with categories\n'+
        '\t\t\t-c has the priority over -r and -f\n'},
    'view': {key: 'v', args: 1, description: 'viewing mode, one of three options:\n'+
        '\t\t\tsnapshot (default) -- still images as a default view,\n'+
        '\t\t\tgifs on hover, video playing/pausing by clicking\n'+
        '\t\t\ton a thumbnail;\n'+
        '\t\t\tanimation -- gifs as a default view, still images\n'+
        '\t\t\ton hover, video playing/pausing by clicking\n'+
        '\t\t\ton a thumbnail;\n'+
        '\t\t\tvideo -- videos as a default view, playing/pausing\n'+
        '\t\t\ton hover on a thumbnail\n'},
    'upd_thumb': {key: 'u', description: 'to update the video thumbnail, if a new snapshot\n'+
        '\t\t\thas been generated more recently\n'}
    //'kaka': {args: 1, mandatory: true}
});
// print usage info
//console.log(ops);
ops.printHelp();

// variables for running shell commands on the wall cluster
// will change for a different user
var userName = 'olutosin'; //tsprf comented
var nodeWebkitProcessName = 'nw'; //tsprf commented

/*** Processing video file names ***/

var range = [];
var fileNames = [];
var extensions = {
    video: '.ogv',
    snapshot: '.jpg',
    animation: '.gif'
};
// default view mode corresponding to extension types
// ***
// snapshot     each clip is represented by a snapshot in a default state
// animation    each clip is represented by a gif in a default state, and by a snapshot on hover
// video        video as shown as a default
// ***
var defaultView = 'snapshot';
var visuals = {};
var categoryLayout = {};
var tags = {};
var pathToVisuals = path.join(__dirname, 'visuals');
// a map of video file names - to keep track of duplicate files
// to test whether the same file is used multiple times when saving snapshots of the video
var videoFilesMap = {};
// timestamped snapshots - needed as thumbnails for segments of the same video
var tsSnapshots = {};
/*** Grid layout configuration ***/
/* layout modes: 
        film holder - frames are arranged in strips from left to right, top to bottom
        snake       - alternating strips of left-to-right followed by strips right-to-left
*/
var layoutMode = 'filmHolder';
//var layoutMode = 'snake';
var hostLetters = ['a', 'b', 'c', 'd'];
var hostIndices = [1, 2, 3, 4];
var screenMatrix = [];
var hostMatrix = {rows: 1, cols: 2};
var hostsLayout = {rows: hostIndices, cols: hostLetters};
//a map of correspondance between the wall clients and grid cells
var client2cell = {};
var cell2client = {};

var snapshotFeedback = {
    waitResponses: {},
    totalSnapshots: 0,
    response: '',
    thumbnail: {}
};
var sortInProgress = false;
var logFileName = null;

// add trim function to String
String.prototype.trim=function(expr){ 
    expr = expr || ' ';
    return this.replace(new RegExp('^['+expr+']'), '').replace(new RegExp('['+expr+']$'), '');
}

// parsing command line options
if(typeof ops.path !== 'undefined') {
    pathToVisuals = ops.path;
    console.log('Reading visuals from '+pathToVisuals);
}
if(typeof ops.view !== 'undefined') {
    if(typeof extensions[ops.view] !== 'undefined') {
        defaultView = ops.view;
    }
    else {
        var str = 'WARNING! Wrong view mode!\n'+
            'Please choose one of the '+Object.keys(extensions).length+
        ' available view modes:';
        for (var key in extensions) {
            str += key+', ';
        }
        str += '\nUsing the default value "'+defaultView+'".\n';
        console.log(str);
        //process.exit();
    }
}
// mutually exclusive options in order of their priority: -c, -f, -r
if(typeof ops.config !== 'undefined') {
    var configFile = path.join(pathToVisuals, ops.config);
    if(!fs.existsSync(configFile))
        console.log('ERROR! Configuration file '+configFile+' does not exist.');
    else {
        console.log('Reading configurations from '+configFile+'\n');
        var data = fs.readFileSync(configFile, 'utf8');
        var configData = {};
        var configFields = {};
        //console.log(data);
        var rows = data.split('\n');
        var item = '';
        for (var i = 0; i<rows.length; i++) {
            //console.log(rows[i]);
            // remove the caret in case the file was saved manually on various systems
            var trimmedRow = rows[i].trim('\r');
            var cols = trimmedRow.split('\t');
            // remove the starting and trailing quotes if the file was saved manually
            // (added by the software automatically)
            item = cols[0].trim('"\'');
            var obj = {};
            for (var j = 0; j<cols.length; j++) {
                if(item.indexOf('***') == 0) {
                    var cat = item.substring(3);
                    if(typeof configFields[cat] == 'undefined') {
                        configData[cat] = [];
                        configFields[cat] = [];
                    }
                    var field = cols[j].trim('"\'');
                    if (field != '')
                        configFields[cat].push(field);
                }
                else {
                    if (j > 0 && typeof configFields[item][j] !== 'undefined') {
                        obj[configFields[item][j]] = cols[j].trim('"\'');
                        //console.log('configData '+item+' '+configFields[item][j]+' '+cols[j]);
                    }
                }
            }
            if (Object.keys(obj).length > 0 && typeof configData[item] !== 'undefined')
                configData[item].push(obj);
        }
        // parsing categories
        if (typeof configData['Categories'] !== 'undefined') {
            for (var k=0; k<configData['Categories'].length; k++) {
                var pos = configData['Categories'][k]['Position'];
                if (typeof categoryLayout[pos] == 'undefined') {
                    categoryLayout[pos] = [];
                }
                categoryLayout[pos].push({
                    name: configData['Categories'][k]['Title'],
                    color: configData['Categories'][k]['Color'],
                    code: configData['Categories'][k]['Code']}
                );
            }
        }
        // parsing file names
        if (typeof configData['Cells'] !== 'undefined') {
            var numberOfViewableCells = hostsLayout.cols.length*
                hostsLayout.rows.length*hostMatrix.cols*hostMatrix.rows;
            for (var k=0; k<configData['Cells'].length; k++) {
                var pos = parseInt(configData['Cells'][k]['Position'], 10);
                if (pos >= 0 && pos < numberOfViewableCells) {
                    // rows of matrix
                    var i = parseInt(pos/(hostsLayout.cols.length*hostMatrix.cols), 10);
                    // columns of matrix
                    var j = pos%(hostsLayout.cols.length*hostMatrix.cols);
                    if (typeof screenMatrix[i] == 'undefined')
                        screenMatrix[i] = [];
                    screenMatrix[i][j] = {id: configData['Cells'][k]['ID']};
                }
                addVisuals(configData['Cells'][k]['ID'], 
                    configData['Cells'][k]['Filename'].replace(extensions.video, ''),
                    configData['Cells'][k]['StartTime'],
                    configData['Cells'][k]['EndTime']
                );
            }
        }
        
        // parsing file names
        if (typeof configData['Tags'] !== 'undefined') {
            for (var k=0; k<configData['Tags'].length; k++) {
                var id = parseInt(configData['Tags'][k]['Video ID'], 10);
                addTag(id, configData['Tags'][k]['Category']);
            }
        }
    }
}

//for (var key in screenMatrix) {
//    for (var k in screenMatrix[key]) {
//        console.log(key+'.' +k + ':');
//        for (var kk in screenMatrix[key][k]) {
//            console.log(kk + ' -> '+screenMatrix[key][k][kk]);
//        }
//    }
//}
//console.log('***visuals');
//for (var key in visuals) {
//    for (var k in visuals[key]) {
//        console.log(key+'.' +k + ': '+visuals[key][k]);
//    }
//}
//console.log('***tags');
//for (var key in tags) {
//    for (var k=0; k<tags[key].length; k++) {
//        console.log(key+'.' +k + ': '+tags[key][k]);
//    }
//}


if(fileNames.length == 0 && typeof ops.files !== 'undefined') {
    //retrieve a map of correspondance between the grid cells and file names
    if (ops.files.indexOf(',') != -1) {
        fileNames = ops.files.split(',');
        for (var i in fileNames) {
            fileNames[i] = fileNames[i].trim();
        }
    }
    else if (ops.files != '')
        fileNames.push(ops.files);
    else {
        console.log('File names are not properly specified!');
        ops.printHelp();
    }
}
else if(fileNames.length == 0 && typeof ops.range !== 'undefined') {
    //retrieve a map of correspondance between the grid cells and file names
    if(ops.range.indexOf('-') != -1) {
        var rng = ops.range.split('-');
        var start = parseInt(rng[0], 10);
        var end = parseInt(rng[1], 10);
        if(end > start)
            for(var i=start; i<=end; i++)
                range.push(i);
        else
            wrongRange();
    }
    else if (ops.range.indexOf(',') != -1) {
        var tempRange = ops.range.split(',');
        for (var i=0; i< tempRange.length; i++) {
            tempRange[i] = tempRange[i].trim();
            var val = parseInt(tempRange[i], 10);
            if (tempRange[i] == '' || !isNaN(val))
                range.push(val);
            else
                wrongRange();
        }
    }
    else if (ops.range != '') {
        ops.range = parseInt(ops.range, 10);
        if(isNaN(ops.range))
            wrongRange();
        else
            range.push(ops.range);
    }
    else
        wrongRange();
}

// check if path to visuals actually exists (synchronously) and is a valid directory
if (fs.statSync(pathToVisuals).isDirectory()) {
    // if we use range of files instead of file names
    if(range.length > 0) {
        var files = fs.readdirSync(pathToVisuals).filter(function(file) {
            if(file.indexOf(extensions.video) != -1) {
                return true;
            }
            return false;
        });
        // important to sort the file names
        files.sort(alphaNumSort);

        for(var i in range) {
            // subtract one because we use natural numbers in a range (i.e. starting from 1)
            var index = range[i] - 1;
            if (typeof files[index] !== 'undefined') {
                //console.log(files[index]);
                fileNames[i] = files[index].replace(extensions.video, '');
            }
        }
    }
    else if (fileNames.length > 0) {
        for (var i in fileNames) {
            if(fileNames[i] != '') {
                var file = fileNames[i]+extensions.video;
                var pathToF = path.join(pathToVisuals, file);
                if(!fs.existsSync(pathToF)) {
                    console.log("No such video file "+pathToF);
                    fileNames[i] = '';
                }
            }
        }
    }
}
else {
    console.log("ERROR! No such directory "+pathToVisuals+"\n");
    //process.exit(0);
}
/*** End of input options processing ***/

//Module dependencies.
var express = require('express')
, http = require('http');
//, url = require('url');
var exec = require("child_process").exec;

var app = express();

// all environments
app.set('port', process.env.PORT || 3000);
app.use(express.favicon());
// this line is necessary for printing GET requests in the console
app.use(express.logger('dev'));
// use bodyParser for file upload - currently not used
//app.use(express.bodyParser());
// adding paths to folders with static files which should be served without additional processing
app.use('/src', express.static(__dirname + '/src'));
app.use('/visuals', express.static(pathToVisuals));

// development only
if ('development' == app.get('env')) {
    app.use(express.errorHandler());
}

//app.get('/visuals', function (req, res) {
//    var queryData = url.parse(req.url, true).query;
//    if(queryData.file)
//        res.sendfile(__dirname + "/visuals/" + queryData.file);
//});

app.get('/', function (req, res) {
    res.sendfile(__dirname + "/control_panel.html");
});

var server = http.createServer(app).listen(app.get('port'), function(){
    console.log('Express server listening on port ' + app.get('port'));
});

// layout of categories
// the order is top to bottom, left to right
// specify colors as hex values as they will be converted to rgba later on
if(Object.keys(categoryLayout).length == 0) {
    categoryLayout = {
        // top row, left to right
        top: [
            {name: 'Category 1', color: '#D9AFFF', code: 'c1'},
            {name: 'Category 2', color: '#9FEFFF', code: 'c2'}
        ],
        // left column, top to bottom
        left : [
            {name: 'Category 3', color: '#19CF76', code: 'c3'}
        ],
        // right column, top to bottom
        right : [
            {name: 'Cat. 4', color: '#FDFF00', code: 'c4'},
            {name: 'Category 5', color: '#FF8A00', code: 'c5'}
        ],
        // bottom row, left to right
        bottom : [
            {name: 'Trash', color: '#F74C4C', code: 'Tr'}
        ]
    };
}

// if screenMatrix hasn't been populated from the configuration file
switch (layoutMode) {
    case 'snake':
        var index = 1;
        for (var row in hostsLayout.rows){
            for (var col in hostsLayout.cols) {
                var host = hostsLayout.cols[col]+hostsLayout.rows[row];
                for (var r=0; r<hostMatrix.rows; r++) {
                    for (var c=0; c<hostMatrix.cols; c++) {
                        var rr = row*hostMatrix.rows+r;
                        // if it's an even row (0, 2nd)
                        if (rr%2==0)
                            var cc = col*hostMatrix.cols+c;
                        else
                            var cc = (hostsLayout.cols.length - col)*hostMatrix.cols - c - 1;
                        if (typeof(screenMatrix[rr]) == 'undefined')
                            screenMatrix[rr] = [];
                        if(typeof(screenMatrix[rr][cc]) == 'undefined')
                            screenMatrix[rr][cc] = {};
                        var clientName = host+'#'+(r*hostMatrix.rows + c + 1)*1;
                        screenMatrix[rr][cc].host = clientName;
                        if (typeof screenMatrix[rr][cc].id == 'undefined') {
                            screenMatrix[rr][cc].id = index;
                            client2cell[clientName] = index;
                            // the indices of video files in fileNames start from 0
                            var fileID = rr*hostsLayout.cols.length*hostMatrix.cols + cc;
                            var fileTitle = typeof fileNames[fileID] !== 'undefined' ? fileNames[fileID] : '';
                            addVisuals(index, fileTitle);
                            index++;
                        }
                        else
                            client2cell[clientName] = screenMatrix[rr][cc].id;
                        //console.log(rr+' '+cc);
                    }
                }
            }
        }
        break;

    default: case 'filmHolder':
        var index = 1;
        for (var row in hostsLayout.rows){
            for (var col in hostsLayout.cols) {
                var host = hostsLayout.cols[col]+hostsLayout.rows[row];
                for (var r=0; r<hostMatrix.rows; r++) {
                    for(var c=0; c<hostMatrix.cols; c++) {
                        var rr = row*hostMatrix.rows+r;
                        var cc = col*hostMatrix.cols+c;
                        if(typeof(screenMatrix[rr]) == 'undefined')
                            screenMatrix[rr] = [];
                        if(typeof(screenMatrix[rr][cc]) == 'undefined')
                            screenMatrix[rr][cc] = {};
                        var clientName = host+'#'+(r*hostMatrix.rows + c + 1)*1;
                        screenMatrix[rr][cc].host = clientName;
                        if (typeof screenMatrix[rr][cc].id == 'undefined') {
                            screenMatrix[rr][cc].id = index;
                            client2cell[clientName] = index;
                            // the indices of video files in fileNames start from 0
                            var fileID = rr*hostsLayout.cols.length*hostMatrix.cols + cc;
                            var fileTitle = typeof fileNames[fileID] !== 'undefined' ? fileNames[fileID] : '';
                            addVisuals(index, fileTitle);
                            index++;
                        }
                        else
                            client2cell[clientName] = screenMatrix[rr][cc].id;
                    }
                }
            }
        }
        break;
}

//for (var key in screenMatrix) {
//    for (var k in screenMatrix[key]) {
//        console.log(key+'.' +k + ':');
//        for (var kk in screenMatrix[key][k]) {
//            console.log(kk + ' -> '+screenMatrix[key][k][kk]);
//        }
//    }
//}
//for (var key in visuals) {
//    for (var k in visuals[key]) {
//        console.log(key+'.' +k + ': '+visuals[key][k]);
//    }
//}

// Initialize sockets communication, both with the clients (the video players)
// and the GUI (the control panel)

var io = require('socket.io').listen(server);
io.set('log level', 1); // reduce logging

var sockets = {};

io.sockets.on('connection', function (socket) {
    var address = socket.handshake.address;
    console.log("New connection from " + address.address + ":" + address.port);
    socket.emit('connected', {
        matrix: screenMatrix, 
        rows: hostsLayout.rows.length*hostMatrix.rows,
        cols: hostsLayout.cols.length*hostMatrix.cols,
        visuals: visuals,
        mode: defaultView,
        categories: categoryLayout,
        tags: tags,
        config: typeof ops.config !== 'undefined' ? ops.config : ''
    });
    
    socket.on('oneWayUpdate', function (data) {
        console.log('oneWayUpdate '+ data.type+ ', element id: '+data.id);
        if (data.type == "selectHelper")
            return;
        // if sort is already in progress (probably, multiple clients trying to sort simultaneously)
        if (data.type == 'sortstart' && sortInProgress) {
            console.log('Sorting in progress. Please try again later.');
            socket.emit('sortFeedback', {response: 'Sorting in progress. Please try again later.'});
            return;
        }
        else if (data.type == 'sortstart' && !sortInProgress)
            sortInProgress = true;
        else if (data.type == 'sortstop')
            sortInProgress = false;
        console.log('host '+ cell2client[data.id]+ ', broadcast '+data.broadcast);
        if(typeof cell2client[data.id] !== 'undefined' && 
                typeof sockets[cell2client[data.id]] !== 'undefined' && !data.broadcast)
            sockets[cell2client[data.id]].emit('oneWayUpdate', data);
        else {
            console.log('broadcasting...');
            socket.broadcast.emit('oneWayUpdate', data);
        }
    });
  
    socket.on('fullUpdate', function (data) {
        console.log('fullUpdate '+data.type);
        switch (data.type) {
            case 'sortupdate':
                // if there are cells to update
                for(var key in data.upd) {
                    // rows of matrix
                    var i = parseInt(data.upd[key].order/(hostsLayout.cols.length*hostMatrix.cols), 10);
                    // columns of matrix
                    var j = data.upd[key].order%(hostsLayout.cols.length*hostMatrix.cols);
                    //console.log('update id '+key+ ' order '+data.upd[key].order+' at '+i+' '+j);
                    screenMatrix[i][j].id = data.upd[key].id;
                    if(typeof cell2client[data.upd[key].id] !== 'undefined') {
                        cell2client[data.upd[key].id] = screenMatrix[i][j].host;
                        client2cell[screenMatrix[i][j].host] = data.upd[key].id;
                        console.log('update: host '+screenMatrix[i][j].host+ ' id '+data.upd[key].id);
                    }
                }
                break;
        }
        socket.broadcast.emit('fullUpdate', data);
    });
  
    socket.on('reloadCells', function (data) {
        console.log("reload cells "+data.cells);
        var hosts = {};
        for (var m=0; m<data.cells.length; m++) {
            // rows of matrix
            var i = parseInt(data.cells[m]/(hostsLayout.cols.length*hostMatrix.cols), 10);
            // columns of matrix
            var j = data.cells[m]%(hostsLayout.cols.length*hostMatrix.cols);
            var h = screenMatrix[i][j].host.split('#');
            hosts[h[0]] = 1;
        }
//        var parentDir = path.resolve(process.cwd(), '..');
//        var commandFile = path.join(parentDir, 'wildo');
        var command = '';
        var instancesToReload = [];
        for (var host in hosts) {
            command = 'ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no'+
                ' -o LogLevel=quiet '+userName+'@'+host+'.wild.lri.fr "killall '+nodeWebkitProcessName+'" &';
//            var command = 'echo "'+userName+'@'+host+'.wild.lri.fr killall '+nodeWebkitProcessName+'"';
            (function (host) {
                exec(command, function (error, stdout, stderr) {
                    // once the callback function called, delete the current host
                    // as we don;t need it anymore and want to decrement the number of hosts
                    // to wait a response from
                    delete hosts[host];
                    // also delete the respective socket
                    delete sockets[host];
                    console.log('reloadCells killall output:');
                    if (error)
                        console.log('error: '+error);
                    else {
                        console.log(stdout);
                        // for each monitor attached to a single host (specified in hostMatrix)
                        for (var i=0; i<hostMatrix.cols*hostMatrix.rows; i++) {
                            instancesToReload.push({host: host, inst: i});
                        }
                    }
                    if (stderr)
                        console.log('stderr: '+stderr);
                    // having received responses from all the hosts 
                    if (Object.keys(hosts).length == 0 && instancesToReload.length > 0)
                        // non-blocking recursive function that runs node-webkit process on each host
                        // with a delay of half a second between each call (to distribute load on the server)
                        recurExecPlay(instancesToReload, 500);
                });
            })(host);
        }
    });
    
    socket.on('clientID', function (data) {
        console.log("socket registered for clientID="+data.clientID);
        //console.log(typeof(data.scr2cell[data.clientID]));
        if (typeof(client2cell[data.clientID]) !== 'undefined') {
            cell2client[client2cell[data.clientID]] = data.clientID;
            console.log('host '+data.clientID+' -- cell '+client2cell[data.clientID]);
        }
        sockets[data.clientID]=socket;
    });

    socket.on('operation', function (data) {
        console.log('received operation '+data.type);
        // additional operations for special events
        if (data.type == 'assignCategory') {
            for (var id in data.extras.cells)
                addTag(id, data.extras.catName);
        }
        else if (data.type == 'kill') {
            console.log('removing sockets...');
            sockets = {};
            cell2client = {};
        }
        
        if(typeof data.id == 'undefined' || data.id == null)
            socket.broadcast.emit(data.type, {extras: data.extras});
        else if (typeof cell2client[data.id] == 'undefined')
            socket.broadcast.emit(data.type, {id: data.id, extras: data.extras});
        else {
            console.log('host '+cell2client[data.id]+' -- cell '+data.id);
            sockets[cell2client[data.id]].emit(data.type, {id: data.id, extras: data.extras});
        }
    });
    
    socket.on('takeSnapshot', function (data) {
        console.log('received: take '+Object.keys(data).length+' snapshots');
        if(Object.keys(data).length == 0) {
            socket.emit('snapshotFeedback', {response: 'Please select videos that you wish to take snapshots of.'});
        }
        else {
            snapshotFeedback.totalSnapshots = Object.keys(data).length;
            // if cells are not aassigned to different clients, i.e. one client displays all cells
            if (Object.keys(cell2client).length == 0) {
                socket.broadcast.emit('takeSnapshot', data);
            }
            else {
                for (var key in data) {
                    if (typeof cell2client[key] != 'undefined') {
                        // pass a message to the respective socket
                        sockets[cell2client[key]].emit('takeSnapshot');
                    }
                }
            }
        }
    });
    
    socket.on('snapshotFeedback', function (data) {
        console.log('received: snapshotFeedback');
        // if there is some result (snapshots data) to process
        if (typeof data.result !== 'undefined' && Object.keys(data.result).length > 0) {
            for (var key in data.result) {
                var ts = data.result[key].ts;
                if (ts >= 0) {
                    // get rid of the header information in the beginning of the content
                    var contentB64Encoded = data.result[key].content.replace('data:image/jpeg;base64,', '');
                    var content = new Buffer(contentB64Encoded, 'base64');//atob() - in client-side javascript
                    var fileName;
                    // if a snapshot does not yet exist for this video
                    if (visuals[key].snapshot == '') {
                        // if it's not a unique video
                        if (videoFilesMap[visuals[key].video] > 1)
                            fileName = visuals[key].video.replace(extensions.video, '')+
                                '_'+key+extensions.snapshot;
                        else
                            fileName = visuals[key].video.replace(extensions.video, extensions.snapshot);
                    }
                    // if the initial snapshot already exists, then save a snapshot 
                    // with a name that has a timestamp in it
                    else
                        fileName = visuals[key].video.replace(extensions.video, '')+
                            '_'+ts+extensions.snapshot;
                    var pathToFile = path.join(pathToVisuals, fileName);
                    (function (vidID, path, file) {
                        saveFile(content, path, function(res) {
                            // if succeeded saving a file
                            if(res === 0) {
                                snapshotFeedback.response += 'Video '+vidID+
                                    ': a snapshot has been saved into '+path+
                                    ' (<a href="visuals/'+file+'" target="_blank">click here to view</a>)\n';
                                if (ops.upd_thumb || visuals[vidID].snapshot == '') {
                                    snapshotFeedback.thumbnail[vidID] = file;
                                    visuals[vidID].snapshot = file;
                                }
                            }
                            else
                                snapshotFeedback.response += 'Video '+vidID+': '+res+'\n';
                            snapshotFeedback.totalSnapshots--;
                            if (snapshotFeedback.totalSnapshots == 0)
                                finalizeSnapshot();
                        });
                    })(key, pathToFile, fileName);
                }
                else {
                    if (ts == -1)
                        snapshotFeedback.response += 'Video '+key+
                            ': please select a frame by playing/pausing a video.\n';
                    else if (ts == -2)
                        snapshotFeedback.response += 'Video '+key+
                            ': cannot take a snapshot of a playing video, please pause it first.\n';
                    // these snapshots will not be taken - wrong timestamp
                    // thus decrement the counter of snapshots
                    snapshotFeedback.totalSnapshots--;
                    if (snapshotFeedback.totalSnapshots == 0)
                        finalizeSnapshot();
                }
            }
        }
    });
    
    function finalizeSnapshot() {
        var data = {
            response: snapshotFeedback.response,
            upd_thumb: snapshotFeedback.thumbnail
        };
        socket.broadcast.emit('snapshotFeedback', data);
        // speical emit for the client who originally sent the snapshotFeedback event
        socket.emit('snapshotFeedback', data);
        snapshotFeedback.thumbnail = {};
        snapshotFeedback.response = '';
    }

    socket.on('save', function (data){
        var pathToFile = path.join(pathToVisuals, data.name);
        var response = '';
        var strForSave = '***Cells\tID\tFilename\tStartTime\tEndTime\tPosition\n';
        for (var row in screenMatrix) {
            var numCols = !numCols ? screenMatrix[row].length : numCols;
            for (var col in screenMatrix[row]) {
                var id = screenMatrix[row][col].id;
                var pos = row*numCols + col*1;
                strForSave += 'Cells\t'+id+'\t'+
                    visuals[id].video+'\t'+
                    (typeof visuals[id].start !== 'undefined' ? visuals[id].start : '')+'\t'+
                    (typeof visuals[id].end !== 'undefined' ? visuals[id].end : '')+'\t'+
                    pos+'\n';
            }
        }
        strForSave += '***Categories\tTitle\tCode\tColor\tPosition\n';
        for (var pos in categoryLayout) {
            for (var i=0; i<categoryLayout[pos].length; i++) {
                strForSave += 'Categories\t'+
                    categoryLayout[pos][i].name+'\t'+
                    categoryLayout[pos][i].code+'\t'+
                    categoryLayout[pos][i].color+'\t'+pos+'\n';
            }
        }
        if(Object.keys(tags).length > 0) {
            strForSave += '***Tags\tVideo ID\tFilename\tCategory\n';
            for (var id in tags) {
                for (var i=0; i<tags[id].length; i++) {
                    strForSave += 'Tags\t'+id+'\t'+
                        visuals[id].video+'\t'+
                        tags[id][i]+'\n';
                }
            }
        }
        if(typeof data.annotations !== 'undefined') {
            strForSave += '***Annotations\tVideo ID\tAnnotation\tStartTime\tEndTime\n';
            strForSave += data.annotations;
        }
        //console.log(strForSave);
        saveFile(strForSave, pathToFile, function (res) {
            if(res === 0)
                response = 'A configuration file has been saved into '+pathToFile+'\n';
            else
                response = 'An error occurred: '+res+'\n';
            socket.emit('saveFeedback', {response: response});
        });
    });
    
    socket.on('startLog', function (data) {
        logFileName = data.number+'.txt';
    });
    
    socket.on('stopLog', function () {
        socket.emit('stopLog', {response: 'Log file '+logFileName+' has been saved.'});
        logFileName = null;
    });
    socket.on('appendLog', function (data) {
        appendLog(data.mes);
    });
    
    socket.on('timeout', function () {
        socket.broadcast.emit('timeout');
    });
});

function appendLog(mes) {
    if (!logFileName)
        return;
    mes = mes || '';
    fs.appendFile(path.join(pathToVisuals, logFileName), mes, function (err) {
        if (err) throw err;
        console.log('append to log: '+mes);
    });
}

// recursive function with a delay for running multiple instances of node-webkit
function recurExecPlay(instances, delay) {
    // get the first instance and shift the queue
    var instance = instances.shift();
    // sleep for "delay" first and then call node-webkit
    // (to distribute load on the server)
    var command = 'ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no'+
    ' -o LogLevel=quiet '+userName+'@'+instance.host+'.wild.lri.fr "'+
    'DISPLAY=:0.'+instance.inst+' '+nodeWebkitProcessName+
    ' video_player 192.168.0.134:3000 '+(instance.inst+1)+'" &';
    console.log('reloadCells using '+nodeWebkitProcessName+' on '+instance.host+'#'+(instance.inst+1));
    exec(command, function (error, stdout, stderr) {
        if (error)
            console.log('error: '+error);
        if (stderr)
            console.log('stderr: '+stderr);
        console.log(stdout);
    });
    
    if (instances.length > 0) {
        setTimeout((function() {
            recurExecPlay(instances, delay);
        }), delay);
    }
}

function convertTimestamp(ts) {
    var newTs = '';
    // if the timestamp contains colons, means it is represented as hours and minutes
    if (ts.indexOf(':') != -1) {
        var t = ts.split(':');
        var len = t.length;
        // seconds may include milliseconds - therefore float
        var seconds = parseFloat(t[len-1], 10);
        var minutes = parseInt(t[len-2], 10);
        var hours = len>2 ? parseInt(t[len-3], 10) : 0;
        newTs = hours*3600 + minutes*60 + seconds;
    }
    else
        newTs = parseFloat(ts, 10);
    newTs = parseFloat(newTs.toFixed(3), 10);
    return newTs;
}

function addTag(id, category) {
    if (typeof tags[id] == 'undefined')
        tags[id] = [];
    tags[id].push(category);
}

function addVisuals(id, file, start, end) {
    visuals[id] = {};
    var fileTitle, pathToF;
    for (var type in extensions) {
        fileTitle = file+extensions[type];
        pathToF = path.join(pathToVisuals, fileTitle);
        if(!fs.existsSync(pathToF)) {
            //console.log("No such file "+pathToF);
            fileTitle = '';
        }
        visuals[id][type] = fileTitle;
    }
    if (file == '')
        return false;
    
    // collecting the map of video files to test whether the same file is used multiple times
    if (visuals[id].video != '')
        if (typeof videoFilesMap[visuals[id].video] == 'undefined')
            videoFilesMap[visuals[id].video] = 1;
        else
            videoFilesMap[visuals[id].video]++;
    // if an appropriate snapshot wasn't found, try a name with an id in it
    if (visuals[id].snapshot == '') {
        fileTitle = file+'_'+id+extensions.snapshot;
        pathToF = path.join(pathToVisuals, fileTitle);
        if(fs.existsSync(pathToF))
            visuals[id].snapshot = fileTitle;
    }
    
    if (start)
        visuals[id].start = convertTimestamp(start);
    if (end)
        visuals[id].end = convertTimestamp(end);
    // if there exist snapshots of the video
    if (typeof tsSnapshots[file] == 'undefined') {
        tsSnapshots[file] = [];
        var reg = new RegExp(file+'_([:.\\d]+)'+extensions.snapshot);
        var snapshots = fs.readdirSync(pathToVisuals).filter(function(item) {
            return reg.test(item);
        });
        for (var i=0; i<snapshots.length; i++) {
            var matches = reg.exec(snapshots[i]);
            tsSnapshots[file].push({
                file: snapshots[i], 
                mtime: fs.statSync(path.join(pathToVisuals, snapshots[i])).mtime.getTime(),
                ts: convertTimestamp(matches[1])
            });
        }
    }
    var maxCtime = 0;
    for (var i=0; i<tsSnapshots[file].length; i++) {
        // if we want to use the most recently taken snapshot as a thumbnail
        if (ops.upd_thumb) {
            if (start && end && tsSnapshots[file][i].ts >= visuals[id].start && 
                tsSnapshots[file][i].ts <= visuals[id].end ||
                start && !end && tsSnapshots[file][i].ts >= visuals[id].start || 
                !start && end && tsSnapshots[file][i].ts <= visuals[id].end ||
                !start && !end) {

                if (maxCtime < tsSnapshots[file][i].mtime) {
                    maxCtime = tsSnapshots[file][i].mtime;
                    visuals[id].snapshot = tsSnapshots[file][i].file;
                }
            }
        }
        else {
            if (start && tsSnapshots[file][i].ts == visuals[id].start)
                visuals[id].snapshot = tsSnapshots[file][i].file;
        }
    }
    return true;
}

function saveFile(content, fileName, callback) {
    // default result
    var result = "There is no data to save!";
    if(content != '') {
        fs.writeFile(fileName, content, function(err) {
            if(err) {
                result = err;
            } else {
                // success
                result = 0;
            }
            if (callback && typeof(callback) === "function") {
                callback(result);
            }
        });
    }
    else if (callback && typeof(callback) === "function") {
        callback(result);
    }
    else
        return result;
}

function alphaNumSort(a, b) {
    if (!isNaN(parseFloat(a, 10)) && !isNaN(parseFloat(b, 10)))
        return parseFloat(a, 10) - parseFloat(b, 10);
    else if (!isNaN(parseFloat(a, 10)))
        return -1;
    else if (!isNaN(parseFloat(b, 10)))
        return 1;
    else {
        a = a.toLowerCase();
        b = b.toLowerCase();
        var aa = a.split(/(\d+)/);
        var bb = b.split(/(\d+)/);
        var length = aa.length > bb.length ? aa.length : bb.length;
        for (var i=0; i<length; i++) {
            if (isFinite(aa[i]) && !isFinite(bb[i]))
                return 1;
            else if (!isFinite(aa[i]) && isFinite(bb[i]))
                return -1;
            else if (!isFinite(aa[i]) && !isFinite(bb[i])) {
                if (aa[i] > bb[i]) return 1;
                if (aa[i] < bb[i]) return -1;
            }
            else {
                aa[i] = parseFloat(aa[i]);
                bb[i] = parseFloat(bb[i]);
                if (aa[i] > bb[i]) return 1;
                if (aa[i] < bb[i]) return -1;
            }
        }
        // comparing in lower case
        return a > b ? 1 : a < b ? -1 : 0;
    }
}

function wrongRange() {
    console.log('Wrong range of files, please specify the beginning and the end of the range properly\n'+
    '(e.g. 1-32 or 1,2,5,...).');
}
