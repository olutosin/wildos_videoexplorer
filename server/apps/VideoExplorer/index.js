// VideoExplorer app - displays a video from an html5 video element
//
// Simple example of application:
// Load a page in the surface and pan/zoom it from the control interface.
//

// Shared modules
var OO = require('OO');
var log = require('Log').shared();

// Server modules
var App = require('../../lib/App');

// Device modules
var Surface = require('../../devices/Surface');

// The `VideoExplorer` class.
var VideoExplorer = App.subclass().name('VideoExplorer')
	.fields({
		
		offsetX: 0,		// Offset of the page from the topleft of the surface
		offsetY: 0,				
		zoom: 1,		// Zoom factor
		//video: null,		
	})
	.constructor(function(config) {
		// *** It looks like we must define a constructor for the ObjectSharer constructor mixin to work
		log.method(this, 'constructor');
		this._super(config);
	})
	.methods({
		initPlatform: function(platform) {
			log.method(this, 'initPlatform');
			this.platform = platform;
			// The path for `injectUIScript` is relative to the url of the document.
			// Since we don't control this, we use an absolute path, based on
			// `this.__dirname`, the absolute path from which the app was loaded.
			//platform.injectJSFile('file://'+this.__dirname+'/jquery.mousewheel.min.js', 'mousewheelJS');
			platform.injectJSFile('file://'+this.__dirname+'/ui.js', 'videoExplorerJS');
		},

		// Called when the app is about to be unloaded.
		stop: function() {
			this._super();

			if (this.platform.window)
				this.platform.window.window.stopVideoExplorer();
		},



		

		// Called by the UI to play the video.
		playVideo: function() {
		/*
			var surface = platform.findDevice({ type: "Surface" });

			surface.mapDevices(function(tile) {
			// do something with the tile
			// tile.name === "<host>_<instance>_<rank>
			// tile.instance, tile.instanceName, tile.tileName
			//
			var col = tile.originX / tile.width; // column number
			var row = tile.originY / tile.height; // row number


			//log.method(this, 'before');

			if(row == 2 && col == 2){
				var doc = tile.window.window.document;
				var video = doc.getElementById('myVideo');
				video.play();

				//log.warn.exit(this, '');
			//log.method(this, 'inside');
			}

			});
		*/
		},

		playPauseAll: function() {

		},

		// Called by the UI to pause the video.
		pauseVideo: function() {
		},

		// Another method to execute the bulk action. it will call the pauseVideo function with the tile id as parameter
		// This will be called by the button in ui.js
		pauseAll: function() {
			//
			//after getting thd platform et all
			// do for each tile
			// then call setTimeout(pauseVideo(tile id), time in milisecs);
		},

		// Called by the UI to move the page within the surface
		panBackgroundBy: function(dX, dY) {
			log.method(this, 'panBackgroundBy', dX, dY);
			this.offsetX += dX;
			this.offsetY += dY;
		},

		// Called by the UI to zoom the page.
		zoomBackgroundBy: function(dZ, x, y) {
			log.method(this, 'zoomBackgroundBy', dZ, x, y);
			this.offsetX = x + (this.offsetX - x) * dZ;
			this.offsetY = y + (this.offsetY - y) * dZ;
			this.zoom *= dZ;
		},
	})
	.shareState('own', 'own',['pauseVideo','playPauseAll'])
;

log.spyMethods(VideoExplorer);

module.exports = VideoExplorer;
